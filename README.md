# CRDL Blacklight

### Developer Information

#### Requirements
+ Ruby 3.2
+ Rails 6.1(optional, will be installed with bundler)
+ Node 12.8
+ Yarn 1.0
+ DLG META project

#### Setup
Instructions for an Ubuntu/Mint Environment
1. Install the [https://gitlab.galileo.usg.edu/DLG/meta](DLG Meta Project)
2. Clone this repo
3. Install ruby 3.2
4. Install bundler with `gem install bundler` (may not be necessary with RVM install)
5. `bundle install` to install gem dependencies 
6. add environment keys to the config/credentials directory (get this from one of the developers)
7. Setup the database with `bundle exec rake db:setup`
8. `bundle exec rails server` to start development web server
9. Go to [http://localhost:3000](localhost:3000)

#### Test Suite
1. `RAILS_ENV=test bundle exec rspec` to run all specs

### License
© 2025 Digital Library of Georgia
