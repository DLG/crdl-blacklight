# Changelog
All notable changes to the DLG CRDL project will be documented in this file.
The format is based on Keep a Changelog.

## [20250221] (Sprint D25)
### Changed
- Carousel Cleanup (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/353)
- CRDL configuration changes (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/313)

## [20250212] (MidSprint D25)
### Changed
- Make hero text a div (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/345)
- Add Blue Hero (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/346)
- Visible focus indicator on Refine section could be improved (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/263)
- h1 hero text (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/348)
- Add homepage title (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/310)

## [20250207] (Sprint C25)
### Changed
- Rails 7 Upgrade (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/258)
- Fix bug that made collections and people search forms use absolute http:// URL. (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/343)

### Staging
- Remove carousel and add hero image (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/307)
- Make hero act like carousel in small sizes (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/342)

## [20250115] (Sprint A25)
### Changed
- Remove twitter/X links (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/304)
- Change Listserve link (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/303)
- Semantic markup for blacklight lists (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/299)
- OCR: Visual list is not marked up as such (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/260)
- The light red color used for focus indicator of the individual accordion sections included on the Institutions page does not meet WCAG minimums for color contrast (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/300)
- The gray color used for some text throughout the website does not meet WCAG minimums for color contrast (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/301)
- Alternative text for linked images could be improved (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/283)
- Heading markup for Searching Help modal could be improved (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/276)
- Focus order should be improved (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/267)
- List markup could be improved (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/269)
- Page title could be improved (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/271)
- Bypass mechanism for alphabet selector could be added to improve accessibility (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/286)
- The alternative text for the Rights statement image included on the Collection page with text does not provide equivalent information (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/297)
- Remove aria-labelledby (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/325)
- The page indicators in the pagination widget for the Collections page are not using aria-lables appropriately (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/298)
- Add solr test data (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/335)

## [20241213] (Sprint Y24)
### Changed
- "All items" and “Read More” are not very descriptive. (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/293)
- Fix heading on inst page (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/302)
- dev deploy is broken due to gem update (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/294)
- Social media icons, search help button, and search button are not labeled. (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/295)
- The all/any <select> element is missing an accessible name (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/292)
- OCR: Image is missing an alt attribute (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/259)
- Decorative images in the Hero section are not marked up as such (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/282)
- Decorative image should be marked up as such (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/284)
- Page title could be improved (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/287)
- Link text could be improved (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/279)
- Page title could be improved (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/264)
- Links could be improved (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/289)
- Update harmful content modal fix (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/313)
- Add role to svgs (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/315)
- Grid/List View links could be improved (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/262)
- Required fields should be indicated in label (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/265)
- Skip to Search link does not work (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/277)
- Skip links are missing on some pages (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/278)
- Turn on autocomplete (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/319)

## [20241101] (Sprint V24)
### Changed
- Update Rails (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/299)

## [20240614] (Sprint U24)

### Changed
- Update Rails to 6.1.7.9 (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/253)

## [20240614] (Sprint L24)
### Changed
- Update msgpack (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/291)
- Update Rails to 6.1.7.8 (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/292)

## [20240517] (Sprint J24)
### Changed
- Turn off auto play of features (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/249)

## [20240209] (Sprint C24)
### Changed
- Show standard 404 page for missing images (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/283)
- Add full size download button to Mirador (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/284)


## [20230922] (Sprint S23)
### Changed
- Linkify URLs (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/275)

## [20230908] (Sprint R23)
### Fixed
- Mirador: selecting page from gallery view does not work (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/240)

## [20230825] (Sprint Q23)
### Changed
- Make /media_types url go to records page with Medium facet expanded and hoisted to the top of the facets list (similar to Educator Resources) (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/207)
- Tweak Solr search field weights and other search behaviors (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/270)


## [20230811] (Sprint P23)
### Changed
- Add informative titles to item pages (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/236)

### Fixed
- Nonexistent collections should be a 404 error (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/225)
- Prevent deploy from failing when passenger is asleep (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/264)

## [20230602] (Sprint K23)
### Changed
- Use custom error pages for 404, 422, and 500 errors. (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/260)

## [20230324] (Sprint F23)
### Changed
- Backport institutions display logic (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/226)
- Speed up retrieval of institutions' collections (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/250)
- Institutions A-Z for non-A-Z inst names (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/229)

### Fixed
- Fix year slider in smaller viewports (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/214)

## [20230210] (Sprint C23)
### Changed
- Remove Constraints customization (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/245)
- Update Rails to 6.1.7.2 (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/245)

## [20230113] (Sprint A23)
### Changed
- Remove non-environment-specific credentials.yml.enc left over from initial commit (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/221)
- Support legacy /cgi/crdl?query=id:... links (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/243)
- Update HTTParty (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/243/diffs?commit_id=7ec9a1ed5a8a74380f16d8e51b83626ade5d887c)

## [20221215] (Sprint Y22)
### Changed
- Update rails-html-sanitizer (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/241)

### Fixed
- Date limiter does not work with open-ended ranges (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/218)
- CRDL API includes whole solr response multiple times (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/219)

## [20221202] (Sprint X22)
### Changed
- 20221108 sn add sentry (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/235)
- Turn sentry on in production (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/236)

## [20220923] (Sprint S22)
### Changed
- Redirects for WSB old export links (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/212)
- Fix /events/events_a-z link (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/213)

## [20220908] (Sprint R22 - First Public Release)
### Changed
- Google analytics for new CRDL interface (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/200)
- Use ci/cd to place different robot.txt per environment (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/203)
- Add secondary title_sort to date sort (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/206)
- Remove banner pointing to old site (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/205)

## [20220829] (Sprint Q22)
### Added
- Sitemap for CRDL (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/183)

### Changed
- Date sort doesn't sort by month and day (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/194)
- Conditionally disable right-click in Mirador (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/198)
- Update date facet so you can enter date range (like DLG) (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/197)
- Change wording for date sort (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/202)


## [20220812] (Sprint P22)
### Added
- create new contact form (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/187)

### Changed
- Change color of facets labels and "show more" so they're more noticeable (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/195)
- Contact form revisited (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/196)
- Replacement images for browse categories (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/199)

### Fixed
- Empty <a href> between badge and Title in Collections/Records (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/174)

## [20220721] (Mid-Sprint O22)
- Update Blacklight to 7.28 (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/192)

## [20220715] (Sprint N22)
- Move type facet to top (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/199)
- Change "Explore" to "Browse" and make homepage headings clickable (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/189)
- Update Rails to 6.1.6.1 (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/190)

### Fixed
- Thumbnail in wrong location on item pages (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/184)

## [20220628] (Mid-Sprint M22)
### Fixed
- Fix "Email Record" by setting default from: address (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/195)
- Prevent record show toolbar from wrapping inelegantly at larger window widths (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/196)

## [20220624] (Mid-Sprint M22)
### Changed
- Accessibility Review for beta launch (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/172)
- Collection thumb link should go to collection page (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/171)
- Toolbar issues (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/167)
- Move 'using CRDL materials' to About (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/177)
- Update footer language (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/175)
- Remove SMS record option (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/176)

### Fixed
- Event show page `<title>`s contain HTML tags (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/173)

## [20220621] (Sprint L22)
### Added
- Social meta tags on show pages with Mirador (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/164)
- Add feedback button (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/170)

### Changed
- Replace default hero image - take 2 (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/155)
- Fix appearance of some buttons in Safari (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/156)
- Fix issues around the Rights and Type facets on advanced search (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/157)
- Replace default hero image - take 3 (FINAL) (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/158)
- A-Z on institution and people browse--exclude empty characters (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/141)
- Update GitLab deployment environment info to new beta URL for prod (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/159)
- Link event title to faceted search on show page (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/169)
- Expand educator resources facet (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/162)
- People show pages: link to individual subjects personal and tweak behavior of some other links (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/165)
- Advanced Search: Move labels to left of boxes to save vertical space (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/163)
- Improve beta banner styles for small screen sizes (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/186)

### Fixed
- Holding institution logos/thumbnails are missing (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/137)
- Mirador query string persists after you navigate away to a new page (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/161)
- Record counts in advanced search people suggestions are not CRDL-specific (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/166)

## [20220609] (Mid-Sprint L22)
### Added
- API: people and events (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/140)
- Add route to support legacy `collections` urls (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/118)
- Add banner with link to legacy production site for soft launch (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/143)

### Changed
- Link person's name to people facet results page (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/146)
- Typeahead for Advanced Search > People (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/147)
- replace current default hero image (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/150)
- Add feature flags for the beta_banner (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/168)
- Hide thumbnails that fail to load (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/151)

### Fixed
- Show page fails to display for items whose record_id contains commas (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/145)


## [20220603] (Sprint K22)
### Change
- Change link to press kit (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/127)
- Upgrade nokogiri to 1.13.6 (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/135)
- Institutions browse page (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/129)
- Search box on Events browse page (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/128)
- More metadata in collection "More About" display (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/136)
- 20220601 sn hide institutions (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/155)
- Add video thumbnails to iiif manifest (DO links) (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/138)

### Fixed
- Remove extra column on People, Institutions and advanced Search pages (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/134)


## [20220520] (Sprint J22)
### Added
- Events and names as attributes to advanced search (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/108)

### Changed
- Change facet name to “Contributing Institution” (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/111)
- Change default number of search results to 12 (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/112)
- Change top section of search help modal to bullets (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/109)
- Make "description" more prominent in Mirador sidebar (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/115)
- Hide download button in Mirador video player (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/116)
- Change press kit for bad links, etc. (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/123)
- API Improvements (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/98)
- Educator Resources facet (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/125)
- Create a more sophisticated fix for multi-video weirdness in Mirador (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/81)
- Hide "Hide image tools" button in Mirador for non-images (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/126)
- Fix facet issue with educator resource facets (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/142)
- Mirador: Fix styles and download issues (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/86)

### Fixed
- Make chosen-js load on advanced search page after navigating with turbolinks (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/114)
- Fix SVG display issue for DLG Logo (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/120)
- Fix SVG display issue for GALILEO Logo (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/122)
- Fix typo in partner page (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/124)
- Layout fixes throughout site (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/113)
- Fix facet issue with educator resource facets (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/142)

## [20220506] (Sprint I22)
### Added
- Add new page under About: Partners (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/88)
- Basic search tips (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/71)
- Advanced search tips (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/72)

### Changed
- Provide search constraints to items and collections menu selections (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/101)
- Open Medium facet on Educator Resources display (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/99)
- Configure UI strings in en.yml (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/102)
- Update link to crdl documentation (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/103)
- Add "Back to Search" on metadata display (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/70)
- Add item counts to institutions index (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/105)
- Add "chosen" to multiselects on Advanced Search (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/96)
- Modify states layer for heatmap to exclude Great Lakes, etc (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/84)
- added scaling and shadow effect when hovering over spotlight images (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/123)
- Update Collections index/show pages (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/78)

### Fixed
- Accessibility Fixes (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/97)
- Fix post-deploy Leaflet CSS issues by using `stylesheet_pack_tag` (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/100)
- Harmful content modal below carousel issue (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/104)
- Add collection icon to 'view=list' results (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/66)
- Dedupe bootstrap in yarn and work around limitations of sassc (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/120)
- added link to application.css (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/121)
- fix spacing issue at bottom of advanced search (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/107)

## [20220422] (Sprint H22)
### Added
- Add 'id:' and 'do:' routes to support legacy bookmarks (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/63)
- Add query routes to support legacy bookmarks (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/64)
- Add 'events/:id' route to support legacy bookmarks (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/62)
- Update Holding Institutions page (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/77)
- Institutions Show Page (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/95)

### Changed
- Update nokogiri to 1.13.4 (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/90)
- Hide thumbnail drawer for single video in Mirador (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/91)
- added height to map (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/97)
- Mirador changes (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/86)
- Update 'Our partners and sponsors' band (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/92)

### Fixed
- Fix results so collection displays (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/94)

## [20220411] (Sprint G22)
### Added
- Add shadow effect to carousel (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/75)
- advanced search (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/53)
- Contributing Institutions (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/80)
- Add video to iiif manifest (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/55)
- Places heatmap (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/57)

### Changed
- Fix issue w/ SavedSearches (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/83)
- Fix manifest urls (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/merge_requests/84)

## [20220225] (Sprint F22)
### Added
- Edit robot.txt do that Google doesn't crawl CRDL staging, dev, prod (temporarily) (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/43)
- Misc: Featured Tabs, Update Rails, Carousel Image (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/73)
- updated default hero image size to 1170x501px (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/51)
- Add Items to People (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/52)
- Change names/event item layout (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/18)
- Institution Browse (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/54)
- Places placeholder (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/56)
- Educator resources (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/46)
- Split out education resources from name/event items (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/19)
- Reorder spotlight circles (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/59)
- Link collection title to collection page in collection record display (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/60)
- Add harmful content button to show page (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/42)
- Carousel image size (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/69)
- Cosmetic change to downcase A-Z in people routes (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/61)
- Adjust text appearance overall (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/68)

### Changed
- Fix border radius on main search (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/49)
- Fix link color & boldness to help them stand out more (https://gitlab.galileo.usg.edu/DLG/crdl-blacklight/-/issues/50)
