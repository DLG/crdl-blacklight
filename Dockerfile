ARG BUILD_STATE=${BUILD_STATE:-rubyonly}

FROM ubuntu:20.04 as base

ENV TZ="Americas/New_York"
ENV DEBIAN_FRONTEND=noninteractive


#build dependencies
RUN apt-get -qq update && apt-get -qq install libreadline-dev libssl-dev libpcre3-dev libexpat1-dev nfs-common build-essential bison zlib1g-dev libxss1 libappindicator1 libindicator7 libcurl4-openssl-dev git libmysqlclient-dev libsqlite3-dev nodejs sudo tzdata wget curl

#ruby args
ARG RUBY_VERSION_MAJOR=3.0
ARG RUBY_VERSION_MINOR=2
ARG RUBY_CONFIG_LINE="--prefix=/app/ruby --disable-install-doc --enable-load-relative --enable-shared"

#install ruby
RUN wget https://cache.ruby-lang.org/pub/ruby/$RUBY_VERSION_MAJOR/ruby-$RUBY_VERSION_MAJOR.$RUBY_VERSION_MINOR.tar.gz && tar xzf ruby-$RUBY_VERSION_MAJOR.$RUBY_VERSION_MINOR.tar.gz && cd ruby-$RUBY_VERSION_MAJOR.$RUBY_VERSION_MINOR && ./configure $RUBY_CONFIG_LINE && make -j$(nproc) && make install && cd .. && rm -rf ruby-$RUBY_VERSION_MAJOR.$RUBY_VERSION_MINOR*

#install psql client
ARG POSTGRESQL_VERSION=11.12
ARG POSTGRESQL_CONFIG_LINE="--with-openssl"
RUN wget https://ftp.postgresql.org/pub/source/v$POSTGRESQL_VERSION/postgresql-$POSTGRESQL_VERSION.tar.gz && tar xzf postgresql-$POSTGRESQL_VERSION.tar.gz && cd postgresql-$POSTGRESQL_VERSION && ./configure $POSTGRESQL_CONFIG_LINE && make -C src/bin install && make -C src/include install && make -C src/interfaces install && cd .. && rm -rf postgresql-$POSTGRESQL_VERSION*

#fix path for ruby and psql 
ENV PATH /app/ruby/bin:/usr/local/pgsql/bin:$PATH
RUN sed -i "s/\/snap\/bin/\/snap\/bin:\/app\/ruby\/bin:\/usr\/local\/pgsql\/bin/" /etc/sudoers

#Add local user, gitlab-runner in this case for continuity w/pipelined servers
#If you have permission issues set the LOCAL_UID to match your local user and rebuild the container.
ARG LOCAL_UID=1000
RUN adduser --uid $LOCAL_UID --gecos "GitLab Runner" --disabled-password gitlab-runner

#Copy necessary files into container
COPY docker_templates/gitlab-runner /etc/sudoers.d/

FROM base AS build-apache
#Apache args
ARG APACHE_VERSION=2.4.51

#install Apache
RUN wget https://ai.galib.uga.edu/files/httpd-$APACHE_VERSION-w-apr.tar.gz && tar xzf httpd-$APACHE_VERSION-w-apr.tar.gz && cd httpd-$APACHE_VERSION && ./configure  '--prefix=/app/apache2' '--with-apxs2=/app/apache2/bin/apxs' '--with-mysqli' '--with-pear' '--with-xsl' '--with-pspell' '--enable-ssl' '--with-gettext' '--with-gd' '--enable-mbstring' '--with-mcrypt' '--enable-soap' '--enable-sockets' '--with-libdir=/lib/i386-linux-gnu' '--with-jpeg-dir=/usr' '--with-png-dir=/usr' '--with-curl' '--with-pdo-mysql' '--enable-so' '--with-included-apr' && make -j$(nproc) && make install && cd ..&& rm -rf httpd-$APACHE_VERSION*
COPY docker_templates/apachestart.sh /startup.sh

#fix path for apache and passenger
ENV PATH /app/apache2/bin:$PATH
RUN sed -i "s/\/app\/ruby\/bin:/\/app\/ruby\/bin:\/app\/apache\/bin:/" /etc/sudoers

#Passenger
RUN gem install -f passenger bundler && passenger-install-apache2-module --snippet | tee /app/apache2/conf/passenger.conf && echo "Include conf/passenger.conf" | sudo tee -a /app/apache2/conf/httpd.conf && passenger-install-apache2-module -a --languages ruby


FROM base AS build-rubyonly
COPY docker_templates/rubystart.sh /startup.sh

FROM build-${BUILD_STATE} AS finish
USER gitlab-runner

CMD ["./startup.sh"]

