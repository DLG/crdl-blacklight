# frozen_string_literal: true

require 'rails_helper'

describe 'Legacy query routes', type: :request do

  context 'redirecting with modified query with /query:' do
    it 'repeats a simple query' do
      get '/query:test'
      expect(response).to redirect_to( '/records?search_field=both&q=test' )
    end

    it 'includes punctuation' do
      get '/query:%22(test)%22'
      expect(response).to redirect_to( '/records?search_field=both&q=%22(test)%22' )
    end

    it 'excludes index labels (kw:, _su:, etc.' do
      get '/query:_ti:%22War+and+Peace%22+au:Leo+Tolstoy'
      expect(response).to redirect_to( '/records?search_field=both&q=%22War+and+Peace%22+Leo+Tolstoy' )
    end
  end

  context 'redirecting with modified query with /cgi/crdl?query=' do
    it 'repeats a simple query' do
      get '/cgi/crdl?query=test'
      expect(response).to redirect_to( '/records?search_field=both&q=test' )
    end

    it 'includes punctuation' do
      get '/cgi/crdl?query=%22(test)%22'
      expect(response).to redirect_to( '/records?search_field=both&q=%22(test)%22' )
    end

    it 'excludes index labels (kw:, _su:, etc.' do
      get '/cgi/crdl?query=_ti:%22War+and+Peace%22+au:Leo+Tolstoy'
      expect(response).to redirect_to( '/records?search_field=both&q=%22War%20and%20Peace%22%20Leo%20Tolstoy' )
    end
  end

end
