# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RecordsController, type: :routing do

  it 'routes to #index' do
    expect(get: '/records').to route_to('records#index')
  end

  it 'routes to #show' do
    expect(get: '/record/1').to route_to(controller: 'records',
                                         action: 'show',
                                         id: '1')
  end

  it 'routes to #email' do
    expect(get: '/record/1/email').to route_to(controller: 'records',
                                               action: 'email',
                                               id: '1')
  end

  it 'routes to #sms' do
    expect(get: '/record/1/sms').to route_to(controller: 'records',
                                             action: 'sms',
                                             id: '1')
  end

  it 'routes to #citation' do
    expect(get: '/record/1/citation').to route_to(controller: 'records',
                                                  action: 'citation',
                                                  id: '1')
  end

  it 'routes to #email' do
    expect(get: '/record/email').to route_to(controller: 'records',
                                             action: 'email')
  end

  it 'routes to #harmful_content' do
    expect(get: '/record/1/harmful_content').to route_to(controller: 'records',
                                                         action: 'harmful_content',
                                                         solr_document_id: '1')
  end

  it 'routes to #advanced_search' do
    expect(get: '/records/advanced').to route_to(controller: 'records',
                                                 action: 'advanced_search')
  end

  it 'routes to #track' do
    expect(post: '/records/1/track').to route_to(controller: 'records',
                                                 action: 'track',
                                                 id: '1')
  end

  it 'routes to #raw' do
    expect(get: '/records/1/raw').to route_to(controller: 'records',
                                              action: 'raw',
                                              format: 'json',
                                              id: '1')
  end

  it 'routes to #opensearch' do
    expect(get: '/records/opensearch').to route_to(controller: 'records',
                                                   action: 'opensearch')
  end

  it 'routes to #suggest' do
    expect(get: '/records/suggest').to route_to(controller: 'records',
                                                   action: 'suggest',
                                                   format: 'json')
  end

  it 'routes to #facet' do
    expect(get: '/records/facet/1').to route_to(controller: 'records',
                                                action: 'facet',
                                                id: '1')
  end

  it 'routes to #range_limit' do
    expect(get: '/records/range_limit').to route_to(controller: 'records',
                                                    action: 'range_limit')
  end

  it 'routes to #index' do
    expect(get: '/collection/1').to route_to(controller: 'records',
                                             action: 'index',
                                             collection_record_id: '1')
  end

  it 'routes to #fulltext' do
    expect(get: '/record/1/fulltext').to route_to(controller: 'records',
                                                  action: 'fulltext',
                                                  solr_document_id: '1')
  end

  it 'routes to #presentation_manifest' do
    expect(get: '/record/1/presentation/manifest').to route_to(controller: 'records',
                                                               action: 'presentation_manifest',
                                                               solr_document_id: '1')
  end

end
