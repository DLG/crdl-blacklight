# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EventsController, type: :routing do

  it 'routes to #index' do
    expect(get: '/events').to route_to('events#index')
  end

  it 'routes to #advanced_search' do
    expect(get: '/events/advanced').to route_to(controller: 'events',
                                                action: 'advanced_search')
  end

  it 'routes to #track' do
    expect(post: '/events/1/track').to route_to(controller: 'events',
                                                action: 'track',
                                                id: '1')
  end

  it 'routes to #raw' do
    expect(get: '/events/1/raw').to route_to(controller: 'events',
                                              action: 'raw',
                                              format: 'json',
                                              id: '1')
  end

  it 'routes to #opensearch' do
    expect(get: '/events/opensearch').to route_to(controller: 'events',
                                                  action: 'opensearch')
  end

  it 'routes to #suggest' do
    expect(get: '/events/suggest').to route_to(controller: 'events',
                                               action: 'suggest',
                                               format: 'json')
  end

  it 'routes to #facet' do
    expect(get: '/events/facet/1').to route_to(controller: 'events',
                                               action: 'facet',
                                               id: '1')
  end

  it 'routes to #range_limit' do
    expect(get: '/events/range_limit').to route_to(controller: 'events',
                                                   action: 'range_limit')
  end

  it 'routes to #range_limit_panel' do
    expect(get: '/events/range_limit_panel/1').to route_to(controller: 'events',
                                                           action: 'range_limit_panel',
                                                           id: '1')
  end

  it 'routes to #email' do
    expect(get: '/events/1/email').to route_to(controller: 'events',
                                               action: 'email',
                                               id: '1')
  end

  it 'routes to #sms' do
    expect(get: '/events/1/sms').to route_to(controller: 'events',
                                             action: 'sms',
                                             id: '1')
  end

  it 'routes to #citation' do
    expect(get: '/events/1/citation').to route_to(controller: 'events',
                                                  action: 'citation',
                                                  id: '1')
  end

  it 'routes to #email' do
    expect(get: '/events/email').to route_to(controller: 'events',
                                             action: 'email')
  end

  it 'routes to #sms' do
    expect(get: '/events/sms').to route_to(controller: 'events',
                                           action: 'sms')
  end

  it 'routes to #citation' do
    expect(get: '/events/citation').to route_to(controller: 'events',
                                                action: 'citation')
  end

  it 'routes to #show' do
    expect(get: '/events/1').to route_to(controller: 'events',
                                         action: 'show',
                                         id: '1')
  end

end
