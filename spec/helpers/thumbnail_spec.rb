# frozen_string_literal: true

require 'rails_helper'

include LinkingHelper

describe ThumbnailHelper do
  describe '#thumbnail_image_tag' do
    it '(Item) contains alt="" attribute' do
      doc = SolrDocument.new( {class_name_ss: 'Item'} )
      img_tag = thumbnail_image_tag(doc)
      expect(img_tag).to include 'alt=""'
    end
    it '(Collection) contains alt="" attribute' do
      doc = SolrDocument.new( {class_name_ss: 'Collection'} )
      img_tag = thumbnail_image_tag(doc)
      expect(img_tag).to include 'alt=""'
    end
    it '(other) contains alt="" attribute' do
      doc = SolrDocument.new
      img_tag = thumbnail_image_tag(doc)
      expect(img_tag).to include 'alt=""'
    end
  end
end
