# frozen_string_literal: true

# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = 'https://crdl.usg.edu/'

SitemapGenerator::Sitemap.create do

  def generate_sitemap_nodes (fl, fq, path, unique_id)
    cursor = '*'
    loop do
      response = Blacklight.default_index.connection.get 'select', params: {
        rows: 1000,
        fl: fl,
        fq: fq,
        cursorMark: cursor,
        sort: 'id asc'
      }
      response['response']['docs'].each do |doc|
        add "/#{path}/#{doc[unique_id]}", lastmod: doc['updated_at_dts'], priority: 0.5
      end
      break if response['nextCursorMark'] == cursor
      cursor = response['nextCursorMark']
    end
  end

  # add static pages
  add '/about/harmful-content', priority: 0.75
  add '/about/overview', priority: 0.75
  add '/about/crdl_api', priority: 0.75
  add '/about/partners', priority: 0.75
  add '/about/steering_committee', priority: 0.75
  add '/teach/using_materials', priority: 0.75
  add '/institutions', priority: 0.75

  # Add item pages
  generate_sitemap_nodes 'id, updated_at_dts', RecordSearchBuilder.item_filters, 'record', 'id'

  # Add people pages
  generate_sitemap_nodes 'id, slug_ss, updated_at_dts', PersonSearchBuilder.filters, 'people', 'slug_ss'

  # Add event pages
  generate_sitemap_nodes 'id, slug_ss, updated_at_dts', EventSearchBuilder.filters, 'events', 'slug_ss'

  # Add collection pages
  generate_sitemap_nodes 'id, record_id_ss, updated_at_dts', CollectionSearchBuilder.filters, 'collections', 'record_id_ss'

end
