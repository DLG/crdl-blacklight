Rails.application.routes.draw do

  mount Blacklight::Engine => '/'
  mount BlacklightAdvancedSearch::Engine => '/'

  root to: 'homepage#index'

  concern :searchable, Blacklight::Routes::Searchable.new
  concern :exportable, Blacklight::Routes::Exportable.new
  concern :range_searchable, BlacklightRangeLimit::Routes::RangeSearchable.new

  resources :solr_documents, only: [:show], path: '/record', controller: 'records' do
    concerns :exportable
    get 'harmful_content', to: 'records#harmful_content'
  end

  resource :records, only: %i[index show] do
    concerns :searchable
    concerns :range_searchable
    collection do
      get 'suggest_subject_personal', defaults: { format: 'json' }
    end
  end

  get '/collection/:collection_record_id',
      to: 'records#index', as: 'collection_home'

  get '/collection/:collection_record_id/:collection_resource_slug',
      to: 'collection_resources#show', as: 'collection_resource'

  resource :collections, only: [:index] do
    get 'map', to: 'map', as: 'map'
    concerns :searchable
    concerns :range_searchable
  end

  get '/collections/:collection_slug',
      to: 'records#index'

  get '/educator_resources', to: 'records#index', as: 'educator_resources', defaults: { f: { educator_resource_b: ['true']} }

  # Providing a blank value for f[medium_facet] will cause the facet to be expanded
  # Custom code in RecordsController looks for media_types parameter to be non-blank and reorders the facet to the top
  # These parameters are not visible to the user until they refine their search further
  get '/media_types', to: 'records#index', as: 'media_types', defaults: { media_types: 1, f: { medium_facet: '', } }

  resource :events, only: %i[index] do
    concerns :searchable
    concerns :range_searchable
  end

  get '/events/events_a-z', to: redirect('/events')

  resources :event_solr_documents, only: [:show], path: '/events', controller: 'events' do
    concerns :exportable
  end

  resource :people, only: %i[index], path: '/people(/:first_char)', first_char: /[a-zA-Z]/, controller: 'people' do
    concerns :searchable
  end

  get '/people/:first_char/:id', first_char: /[a-zA-Z]/, to: redirect('/people/%{id}')

  resources :people_solr_documents, only: [:show], path: '/people', controller: 'people' do
    concerns :exportable
  end

  resource :institutions, only: %i[index], path: '/institutions(/:first_char)', first_char: /[a-zA-Z]|0-9/, controller: 'institutions' do
    concerns :searchable
  end

  resources :institution_solr_documents, only: [:show], path: '/institutions/', controller: 'institutions' do
    concerns :exportable
  end

  resources :collection_solr_documents, only: [:show], path: '/collections/', controller: 'collections' do
    concerns :exportable
  end

  get '/search', to: 'advanced#index', as: 'search'

  namespace :teach do
    get 'using_materials'
  end

  namespace :about do
    get 'overview'
    get 'crdl_api'
    get 'partners'
    get 'steering_committee'
    get 'harmful-content'
    get 'harmful-content-form'
    post 'harmful_content_submit'
  end

  namespace :help do
    get 'search'
    get 'refine-items'
    get 'refine-collections'
    get 'map'
  end

  resources :solr_documents, only: [:show], path: '/record', controller: 'records' do
    concerns :exportable
    get 'fulltext', to: 'records#fulltext'
    get 'presentation/manifest', to: 'records#presentation_manifest'
  end

  resources :bookmarks do
    concerns :exportable
    collection do
      delete 'clear'
    end
  end

  resources :places, only: %i[index]

  get '/iiif-viewer', to: 'iiif_viewer#iiif_viewer', as: 'iiif_viewer'
  get '/iiif-embeddable', to: 'iiif_viewer#embeddable_iiif_viewer', as: 'embeddable_iiif_viewer'

  get :contact, to: 'contact#index'

  # legacy routes
  get '/query:(*query)', to: redirect { |path_params, req|
      q = path_params[:query]&.gsub(/[_a-zA-Z0-9]+:/,'')  # remove index labels, e.g., kw:, _su:
      "/records?search_field=both&q=#{q}"
  }
  get '/cgi/crdl', to: redirect { |path_params, req|
    query = req.query_parameters[:query]
    if query =~ /^id:([\w,-]+)$/
      "/record/#{$1}"
    else
      q = query&.gsub(/[_a-zA-Z0-9]+:/,'')  # remove index labels, e.g., kw:, _su:
      "/records?search_field=both&q=#{q}"
    end
  }

  # legacy topics
  get '/topics/community_organizing', to: redirect('/records?f_inclusive%5Bsubject_facet%5D%5B%5D=Mass+meetings&f_inclusive%5Bsubject_facet%5D%5B%5D=Workshops+(Adult+education)&f_inclusive%5Bsubject_facet%5D%5B%5D=Protest+marches')
  get '/topics/culture_of_the_movement', to: redirect('/records?f_inclusive%5Bsubject_facet%5D%5B%5D=African+Americans--Songs+and+music&f_inclusive%5Bsubject_facet%5D%5B%5D=Black+power&f_inclusive%5Bsubject_facet%5D%5B%5D=Nonviolence&f_inclusive%5Bsubject_facet%5D%5B%5D=Passive+resistance&f_inclusive%5Bsubject_facet%5D%5B%5D=Protest+songs&f_inclusive%5Bsubject_facet%5D%5B%5D=Public+worship&f_inclusive%5Bsubject_facet%5D%5B%5D=Religion+and+politics&f_inclusive%5Bsubject_facet%5D%5B%5D=Singing')
  get '/topics/organizations', to: redirect('/records?f_inclusive%5Bsubject_facet%5D%5B%5D=Alabama+Christian+Movement+for+Human+Rights&f_inclusive%5Bsubject_facet%5D%5B%5D=Albany+Movement+(Albany,+Ga.)&f_inclusive%5Bsubject_facet%5D%5B%5D=American+Civil+Liberties+Union&f_inclusive%5Bsubject_facet%5D%5B%5D=Black+Coordinating+Committee+(Rome,+Ga.)&f_inclusive%5Bsubject_facet%5D%5B%5D=Committee+on+Appeal+for+Human+Rights&f_inclusive%5Bsubject_facet%5D%5B%5D=Community+Affairs+Committee+(Birmingham,+Ala.)&f_inclusive%5Bsubject_facet%5D%5B%5D=Congress+of+Racial+Equality&f_inclusive%5Bsubject_facet%5D%5B%5D=Council+of+Federated+Organizations+(U.S.)&f_inclusive%5Bsubject_facet%5D%5B%5D=Freedom+Singers+(Musical+group+:+Albany,+Ga.)&f_inclusive%5Bsubject_facet%5D%5B%5D=Highlander+Folk+School+(Monteagle,+Tenn.)&f_inclusive%5Bsubject_facet%5D%5B%5D=Highlander+Research+and+Education+Center+(Knoxville,+Tenn.)&f_inclusive%5Bsubject_facet%5D%5B%5D=International+Union+of+Mine,+Mill,+and+Smelter+Workers&f_inclusive%5Bsubject_facet%5D%5B%5D=Kappa+Alpha+Order.+Gamma+Chapter,+University+of+Georgia&f_inclusive%5Bsubject_facet%5D%5B%5D=Ku+Klux+Klan+(1915-+)&f_inclusive%5Bsubject_facet%5D%5B%5D=Macon+White+Citizens+Council+for+the+Betterment+of+America+(Macon,+Ga.)&f_inclusive%5Bsubject_facet%5D%5B%5D=NAACP+Legal+Defense+and+Educational+Fund&f_inclusive%5Bsubject_facet%5D%5B%5D=Nation+of+Islam+(Chicago,+Ill.)&f_inclusive%5Bsubject_facet%5D%5B%5D=National+Association+for+the+Advancement+of+Colored+People&f_inclusive%5Bsubject_facet%5D%5B%5D=S.C.L.C.+Operation+Breadbasket&f_inclusive%5Bsubject_facet%5D%5B%5D=Southern+Christian+Leadership+Conference&f_inclusive%5Bsubject_facet%5D%5B%5D=Student+Nonviolent+Coordinating+Committee+(U.S.)&f_inclusive%5Bsubject_facet%5D%5B%5D=Summer+Community+Organization+and+Political+Education+(Organization)&f_inclusive%5Bsubject_facet%5D%5B%5D=Summit+Leadership+Conference+(Atlanta,+Ga.)&f_inclusive%5Bsubject_facet%5D%5B%5D=Sumter+County+Movement+(Americus,+Ga.)')
  get '/topics/white_resistance', to: redirect('/records?f_inclusive%5Bsubject_facet%5D%5B%5D=Bombings&f_inclusive%5Bsubject_facet%5D%5B%5D=Communism&f_inclusive%5Bsubject_facet%5D%5B%5D=Executions+in+effigy&f_inclusive%5Bsubject_facet%5D%5B%5D=Georgia.+General+Assembly.+House+of+Representatives.+Committee+on+education&f_inclusive%5Bsubject_facet%5D%5B%5D=Georgia.+General+Assembly.+House+of+Representatives.+Committee+on+the+State+of+the+Republic&f_inclusive%5Bsubject_facet%5D%5B%5D=Georgia.+Voter+Registration+Board&f_inclusive%5Bsubject_facet%5D%5B%5D=Government,+Resistance+to&f_inclusive%5Bsubject_facet%5D%5B%5D=Hate+crimes&f_inclusive%5Bsubject_facet%5D%5B%5D=Imprisonment&f_inclusive%5Bsubject_facet%5D%5B%5D=Incendiary+bombs&f_inclusive%5Bsubject_facet%5D%5B%5D=Indictments&f_inclusive%5Bsubject_facet%5D%5B%5D=Injunctions&f_inclusive%5Bsubject_facet%5D%5B%5D=Intervention+(Federal+government)&f_inclusive%5Bsubject_facet%5D%5B%5D=Intimidation&f_inclusive%5Bsubject_facet%5D%5B%5D=Ku+Klux+Klan+(1915-+)&f_inclusive%5Bsubject_facet%5D%5B%5D=Offences+against+property&f_inclusive%5Bsubject_facet%5D%5B%5D=Restraining+orders&f_inclusive%5Bsubject_facet%5D%5B%5D=Segregationists&f_inclusive%5Bsubject_facet%5D%5B%5D=School+integration--Massive+resistance+movement&f_inclusive%5Bsubject_facet%5D%5B%5D=States+rights&f_inclusive%5Bsubject_facet%5D%5B%5D=Student+suspension&f_inclusive%5Bsubject_facet%5D%5B%5D=Subversive+activities&f_inclusive%5Bsubject_facet%5D%5B%5D=Tax+evasion--Investigation&f_inclusive%5Bsubject_facet%5D%5B%5D=Trials+(sedition)&f_inclusive%5Bsubject_facet%5D%5B%5D=White+Citizens+Council+(Ala.)&f_inclusive%5Bsubject_facet%5D%5B%5D=White+Citizen+councils')
  get '/topics/boycott_direct_action', to: redirect('/records?f_inclusive%5Bsubject_facet%5D%5B%5D=Boycotts&f_inclusive%5Bsubject_facet%5D%5B%5D=Car+pools&f_inclusive%5Bsubject_facet%5D%5B%5D=Direct+action&f_inclusive%5Bsubject_facet%5D%5B%5D=Kneel-ins&f_inclusive%5Bsubject_facet%5D%5B%5D=Passive+resistance&f_inclusive%5Bsubject_facet%5D%5B%5D=Picketing&f_inclusive%5Bsubject_facet%5D%5B%5D=Sit-ins&f_inclusive%5Bsubject_facet%5D%5B%5D=Strikes+and+lockouts')
  get '/topics/economic_justice', to: redirect('/records?f_inclusive%5Bsubject_facet%5D%5B%5D=Discrimination+in+employment&f_inclusive%5Bsubject_facet%5D%5B%5D=S.C.L.C.+Operation+Breadbasket&f_inclusive%5Bsubject_facet%5D%5B%5D=Discrimination+in+housing')
  get '/topics/legal_strategies', to: redirect('/records?f_inclusive%5Bsubject_facet%5D%5B%5D=Indictments&f_inclusive%5Bsubject_facet%5D%5B%5D=Injunctions&f_inclusive%5Bsubject_facet%5D%5B%5D=Restraining+orders&f_inclusive%5Bsubject_facet%5D%5B%5D=School+integration--Massive+resistance+movement')
  get '/topics/mass_protest', to: redirect('/records?f_inclusive%5Bsubject_facet%5D%5B%5D=Civil+rights+demonstrations&f_inclusive%5Bsubject_facet%5D%5B%5D=Protest+marches')
  get '/topics/school_desegregation', to: redirect('/records?f_inclusive%5Bsubject_facet%5D%5B%5D=Central+High+School+(Little+Rock,+Ark.)&f_inclusive%5Bsubject_facet%5D%5B%5D=College+integration&f_inclusive%5Bsubject_facet%5D%5B%5D=Georgia.+General+Assembly.+House+of+Representatives.+Committee+on+education&f_inclusive%5Bsubject_facet%5D%5B%5D=Georgia+Institute+of+Technology&f_inclusive%5Bsubject_facet%5D%5B%5D=School+integration&f_inclusive%5Bsubject_facet%5D%5B%5D=School+integration--Massive+resistance+movement&f_inclusive%5Bsubject_facet%5D%5B%5D=Segregation+in+education&f_inclusive%5Bsubject_facet%5D%5B%5D=University+of+Alabama&f_inclusive%5Bsubject_facet%5D%5B%5D=University+of+Georgia&f_inclusive%5Bsubject_facet%5D%5B%5D=University+of+Mississippi&f_inclusive%5Bsubject_facet%5D%5B%5D=University+System+of+Georgia&f_inclusive%5Bsubject_facet%5D%5B%5D=University+System+of+Georgia.+Board+of+Regents')
  get '/topics/voting_rights', to: redirect('/records?f_inclusive%5Bsubject_facet%5D%5B%5D=African+Americans--Suffrage&f_inclusive%5Bsubject_facet%5D%5B%5D=Literacy+tests+(Election+law)&f_inclusive%5Bsubject_facet%5D%5B%5D=Voter+Education+Project+(Southern+Regional+Council)&f_inclusive%5Bsubject_facet%5D%5B%5D=Voter+registration&f_inclusive%5Bsubject_facet%5D%5B%5D=Voting')

  #redirects
  get '/topics(/:anything)', to: redirect('/'), status: 301
  get '/help(/:anything)', to: redirect('/'), status: 301
  get '/about', to: redirect('/about/overview'), status: 301
  get '/about/steering', to: redirect('/about/steering_committee'), status: 301
  get '/about/press_kit', to: redirect('https://sites.google.com/view/dlg-docs/resources/promotional-materials/crdl-2022-press-kit'), status: 301
  get '/resources', to: redirect('/educator_resources'), status: 301
  get '/media_types', to: redirect('/records?f%5Bmedium_facet%5D%5B%5D=')
  get '/places/:anything', to: redirect('/places'), status: 301
  get '/cgi(/:anything)', to: redirect('/'), status: 301
  get '/export/html/ugabma/wsbn/crdl_ugabma_wsbn_:id', to: redirect('/record/ugabma_wsbn_wsbn%{id}.html'), id: /\d+/, status: 301
  get '/export/html/:repo/:collection/crdl_:record_id', to: redirect('/record/%{record_id}.html'), status: 301
  get '/export/xml/:repo/:collection/crdl_:record_id', to: redirect('/record/%{record_id}.xml'), status: 301
  get '/id::record_id', to: redirect('/record/%{record_id}'), status: 301
  get '/do::record_id', to: redirect('/record/%{record_id}'), status: 301

  match '/404', to: 'errors#file_not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all
end
