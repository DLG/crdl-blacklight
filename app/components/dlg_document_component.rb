# frozen_string_literal: true

class DlgDocumentComponent < Blacklight::DocumentComponent

  def title(*args, **kwargs)
    super(*args, component: DocumentTitleComponentWithBadges, **kwargs)
  end

end