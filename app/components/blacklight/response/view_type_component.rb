# frozen_string_literal: true

module Blacklight
  module Response
    # Render spellcheck results for a search query
    class ViewTypeComponent < ViewComponent::Base
      renders_many :views, 'Blacklight::Response::ViewTypeButtonComponent'

      # @param [Blacklight::Response] response
      def initialize(response:, views: {}, search_state:, selected: nil)
        @response = response
        @views = views
        @search_state = search_state
        @selected = selected
      end

      def before_render
        return if views.any?

        if @views[:gallery]
          view(key: :gallery, view: @views[:gallery], selected: @selected == :gallery, search_state: @search_state)
        end
        if @views[:list]
          view(key: :list, view: @views[:list], selected: @selected == :list, search_state: @search_state)
        end
      end

      def render?
        Deprecation.silence(Blacklight::ConfigurationHelperBehavior) do
          @view_context.has_alternative_views?
        end
      end
    end
  end
end
