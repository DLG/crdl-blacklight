# frozen_string_literal: true

class DocumentTitleComponentWithBadges < Blacklight::DocumentTitleComponent
  def title
    document_link = if presenter.document.klass == 'Collection'
                      helpers.collection_index_link_to presenter.document
                    elsif presenter.document.klass == 'Serial'
                      helpers.serial_index_link_to presenter.document
                    else
                      super
                    end
    # don't use "#{}" because it won't preserve HTML-safe status
    "".html_safe + collection_icon + fulltext_icon + document_link
  end

  def collection_icon
    return unless @document.collection?
    helpers.collection_icon(document = @document)
  end

  def fulltext_icon
    return unless @document.item? && @document.fulltext
    helpers.fulltext_icon(document = @document)
  end

end