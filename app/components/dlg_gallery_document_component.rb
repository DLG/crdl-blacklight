# frozen_string_literal: true

class DlgGalleryDocumentComponent < Blacklight::Gallery::DocumentComponent

  def title(*args, **kwargs)
    super(*args, component: DocumentTitleComponentWithBadges, **kwargs)
  end

end