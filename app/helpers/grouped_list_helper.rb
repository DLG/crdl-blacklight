# frozen_string_literal: true

# helper methods for grouped lists of items for browse pages
module GroupedListHelper
  def collection_grouped_list(blacklight_info, link_params)
    items = blacklight_info[:value]
    total_item_count = items.size
    grouped = items.group_by { |item| [item.collection_title.gsub(/\W+/, ' ').strip, # punctuation removed for sorting
                                       item.collection_title,
                                       item.collection_id] }
    collections = grouped.sort.map do |collection, items|
      _sort_title, collection_title, collection_id = collection
      collection_provenance = sorted_separated_list items, :dcterms_provenance_display
      [collection_id, collection_title, collection_provenance, items.sort_by(&:title)]
    end
    record_config = RecordsController.blacklight_config
    render partial: 'shared/grouped_item_list_gallery', locals: { grouped_list: collections,
                                                                  record_config: record_config,
                                                                  total_item_count: total_item_count,
                                                                  link_params: link_params,
                                                                  items_per_collection: 6 } if grouped.present?
  end

  def sorted_separated_list(items, column)
    items.map{ |x| x[column] }.flatten.compact.group_by{ |x| x }.map{|k, v| [v.count, k] }
         .sort.reverse.map(&:last).join(' • ')
  end

  def render_related_document(doc, config)
    presenter = Blacklight::IndexPresenter.new(doc, self, config)
    doc.override_blacklight_linking = true
    rendered = render Blacklight::Gallery::DocumentComponent.new(presenter: presenter)
    doc.override_blacklight_linking = false
    rendered
  end

  # The below four methods only differ in the facet values they feed into their "Show More" links
  def event_item_list_non_educator(blacklight_info)
    collection_grouped_list(blacklight_info, {
      'f[event_title_sms][]' => blacklight_info[:document]&.title
    })
  end

  def event_item_list_educator(blacklight_info)
    collection_grouped_list(blacklight_info, {
      'f[event_title_sms][]' => blacklight_info[:document]&.title,
      'f[educator_resource_b][]' => true
    })
  end

  def person_item_list_non_educator(blacklight_info)
    collection_grouped_list(blacklight_info, {
      'f[name_authoritative_sms][]' => blacklight_info[:document]&.authoritative_name
    })
  end

  def person_item_list_educator(blacklight_info)
    collection_grouped_list(blacklight_info, {
      'f[name_authoritative_sms][]' => blacklight_info[:document]&.authoritative_name,
      'f[educator_resource_b][]' => true
    })
  end
end
