# frozen_string_literal: true

# Common helper methods for HTML link/button generating logic
module LinkingHelper
  def freely_available_path
    '/records?f%5Bclass_name%5D%5B%5D=Item&f_inclusive%5Brights_facet%5D%5B%5D=http%3A%2F%2Frightsstatements.org%2Fvocab%2FInC-NC%2F1.0%2F&f_inclusive%5Brights_facet%5D%5B%5D=http%3A%2F%2Frightsstatements.org%2Fvocab%2FInC-RUU%2F1.0%2F&f_inclusive%5Brights_facet%5D%5B%5D=http%3A%2F%2Frightsstatements.org%2Fvocab%2FNKC%2F1.0%2F&f_inclusive%5Brights_facet%5D%5B%5D=http%3A%2F%2Frightsstatements.org%2Fvocab%2FNoC-NC%2F1.0%2F&f_inclusive%5Brights_facet%5D%5B%5D=http%3A%2F%2Frightsstatements.org%2Fvocab%2FNoC-US%2F1.0%2F&f_inclusive%5Brights_facet%5D%5B%5D=https%3A%2F%2Fcreativecommons.org%2Flicenses%2Fby-nc%2F4.0%2F&f_inclusive%5Brights_facet%5D%5B%5D=https%3A%2F%2Fcreativecommons.org%2Flicenses%2Fby%2F4.0%2F&search_field%3Dadvanced'
  end

  # handle linking in catalog results
  def linkify(options = {})
    url = options[:value].first
    link_to url, url, target: '_blank'
  end

  def regex_linkify(options = {})
    values = options[:value]
    values.map! do |v|
      URI.extract(v).each do |uri|
        v = v.sub(uri, link_to(uri, uri))
      end
      v
    end
    values.join('<br>').html_safe
  end

  def server_image_url(relative_path)
    return no_thumb_url if relative_path.blank?

    image_host = if relative_path.starts_with? '/uploads'
                   Rails.configuration.dlg_admin_url
                 else
                   Rails.configuration.image_server
                 end
    URI.join(image_host, relative_path).to_s
  end

  # generate a link to a collection page using a collection document
  def link_to_collection_page(options)
    doc = options[:document]
    return doc.title unless doc.collection?
    link_to doc.title, collection_home_path(doc.id)
  end

  # generate link to collection page from an item document
  def item_link_to_collection_page(options)
    doc = options[:document]
    link_to doc.collection_title, collection_home_path(doc.collection_id)
  end

  # overrides function in BL configuration_helper_behavior
  def collection_index_link_to(document)
    link_title = if document.key? 'title'
                   document.title
                 else
                   I18n.t('collection.homepage_link')
                 end
    link_to link_title, collection_home_path(document.id), class: 'record-show-link'
  end

  # link to external site from a collection homepage
  def collection_external_homepage_link
    link_to(
      I18n.t('collection.homepage_link'),
      @collection.edm_is_shown_at.first,
      class: 'btn btn-primary', target: '_blank'
    )
  end

  # show button for link to external collection page
  # TODO: check for local? value and display something else? not 'Partner'?
  def visit_partner_button(document)
    if document.md_url
      link_to(
        I18n.t('record.view_at_partner_site'),
        document.md_url,
        class: 'btn btn-primary btn-block', target: '_blank'
      )
    else
      ''
    end
  end

  # render an individual RS link image
  def rights_statement_icon_link(rs_data)
    link_to(
      image_tag(rs_data[1][:icon_url], class: 'rights-statement-icon', alt: rs_data[1][:label]),
      rs_data[1][:uri], class: 'rights-statement-link', target: '_blank', rel: 'noopener'
    )
  end

  # render an icon for each RS associated with a collection
  def collection_rights_icons(rights_array)
    image_tags = []
    I18n.t([:rights])[0].each do |r|
      rights_array.each do |rights|
        next unless r[1][:uri] == rights
        image_tags << rights_statement_icon_link(r)
      end
    end
    image_tags.join('').html_safe
  end

  # render icon for RS value
  # TODO: refactor/reconider use of I18n file for this purpose
  def rights_icon_tag(obj)
    I18n.t([:rights])[0].each do |r|
      return rights_statement_icon_link(r) if r[1][:uri] == obj[:value].first
    end
    link_to obj[:value].first, obj[:value].first
  end

  def linkify_person_slugs(options = {})
    first_char = options[:document]&.first_char
    links = options[:value]&.map { |slug| link_to slug, people_solr_document_path(first_char, slug)}
    links.join('<br>').html_safe
  end

  def linkify_subjects_personal(options)
    options[:value]
      .map {|subj| link_to subj, search_records_path("f[subject_personal_facet][]": subj) }
      .join('<br>').html_safe
  end

  # generate a link to a result page using a person's name
  def link_to_people_facet_results(options)
    authoritative_name = options[:value].first
    link_to authoritative_name, search_records_path("f[subject_personal_facet][]": authoritative_name)
  end

  # generate a link to a result page using a person's name
  def link_to_event_facet_results(options)
    title = options[:value].first
    # link_to authoritative_name, "#{search_records_path}?f%5Bsubject_personal_facet%5D%5B%5D=#{authoritative_name}"
    link_to title, search_records_path("f[event_title_sms][]": title)
  end

  # Find all urls and linkify for a blacklight document
  def linkify_urls(options = {})
    new_values= options[:value].map {|str| find_urls_and_linkify(str)}
    new_values.join('<br>').html_safe
  end

  # Find all urls and linkify for a blacklight document for the first value
  def linkify_urls_single(options = {})
    str = options[:value].first
    find_urls_and_linkify(str).html_safe
  end

  # Find all urls in a string and replace with anchor tags
  def find_urls_and_linkify(str)
    return str if str.blank?
    urls = URI.extract(strip_links(str), %w[http https]) #look for urls and disregard existing anchor tags
    url_map = urls.to_h {|url| [url, link_to(url, url, target: '_blank')]}
    url_map.each {|k, v| str.sub!(k, v)}

    sanitize(str, tags: %w(a), attributes: %w(href target)).html_safe
  end
end
