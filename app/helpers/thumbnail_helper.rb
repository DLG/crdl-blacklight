# frozen_string_literal: true

# Helpers for Thumbnails
module ThumbnailHelper

  # Return img tag for thumbnail with link
  def thumbnail_image_tag(document, options = {})
    options[:css] ||= 'thumbnail img-fluid'
    case document.klass
    when 'Item'
      image_tag(item_thumb_url(document), class: options[:css], alt: '')
    when 'Collection'
      image_tag(collection_thumb_url(document), class: "#{options[:css]} collection-image", alt: '')
    else
      image_tag(no_thumb_url, class: options[:css], alt: '')
    end
  end

  # Generate URL for Item or use standard Audio file icon
  def item_thumb_url(document)
    return no_thumb_url unless document.item?

    if document.types&.include?('Sound')
      asset_path 'file-audio.png'
    else
      server_image_url("/thumbnails/#{document['repository_slug_ss']}/#{document['collection_slug_ss']}/#{document['record_id_ss']}.jpg")
    end
  end

  def show_item_thumb(document)
    if document.do_url
      link_to(
        thumbnail_image_tag(document, css: ''),
        document.do_url
      )
    else
      thumbnail_image_tag(document, css: '') + visit_partner_button(document)
    end
  end

  # Check if collection has image set, return it or old style thumbnail URL
  def collection_thumb_url(document)
    return no_thumb_url unless document.collection?

    if document.image
      server_image_url document.image
    elsif document.holding_institution_image
      document.holding_institution_image
    else
      no_thumb_url
    end
  end

  # Link index page item thumb to DO if present, show page otherwise
  def index_item_thumb(document)
    if document.do_url
      link_to(
        thumbnail_image_tag(document),
        url_for_document(document)
      )
    else
      link_to(
        thumbnail_image_tag(document),
        solr_document_path(document.id)
      )
    end
  end

  def no_thumb_url
    asset_path 'no-thumb.png'
  end

  def sound_type_item?(document)
    document.types&.include?('Sound') &&
      document.item?
  end
end
