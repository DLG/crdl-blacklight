module BannerHelper
  def outage_banner(startDate, endDate, message)
    today = Date.current
    startDate = Date.parse(startDate)
    endDate = Date.parse(endDate)
    if today.between?(startDate,endDate)
      message = "<div class=\"banner alert-danger\">
                <div class=\"container\">
                  <div class=\"row\">
                    <div class=\"col-sm-12 text-center\">
                      #{message}
                    </div>
                  </div>
                </div>
              </div>"
      message.html_safe
    end
  end
end