# frozen_string_literal: true

# Common helper methods for facet-related stuff
module DlgFacetsHelper
  def boolean_facet_labels(value)
    value == 'true' ? 'Yes' : 'No'
  end

  def active_facet_sort(field, value)
    sort = @sort_params[field] || 'index'
    sort == value ? 'btn-primary' : 'btn-default'
  end

  def sort_params
    @sort_params
  end

  def primary_sort_param
    @primary_sort_param
  end

  def secondary_sort_param
    @secondary_sort_param
  end

  def advanced_search_facets_with_suggest_service
    { 'subject_personal_facet' => suggest_subject_personal_records_path }
  end

  # @param [String] facet_name
  # @param [String, Blacklight::Solr::Response::Facets::FacetItem] facet_item
  # @return [String] label for facet item, honoring the facet's helper_method (from blacklight_config) if present
  def label_for_facet_item(facet_name, facet_item)
    facet_config = facet_configuration_for_field(facet_name)
    # Return if no helper method. facet_item_presenter(...).label correctly handles this scenario but is slower.
    unless facet_config.helper_method.present?
      return facet_item.is_a?(String) ? facet_item : facet_item.value
    end

    if facet_item.is_a? String
      Blacklight::Solr::Response::Facets::FacetItem.new(facet_item)
    end
    facet_item_presenter(facet_config, facet_item, facet_name).label
  end
end