# frozen_string_literal: true

# Blacklight search controller for Collections
class PeopleController < CatalogController
  include HasCustomSolrGetter

  before_action :set_first_character, only: [:index]
  before_action :set_a_to_z

  layout 'blacklight'

  configure_blacklight do |config|
    config.search_builder_class = PersonSearchBuilder
    config.document_model = PersonSolrDocument
    config.show.route = { controller: 'people' }

    # solr fields to be displayed in the index (search results) view
    #   The ordering of the field names is the order of the display
    config.add_index_field :authoritative_name,      label: I18n.t('person.fields.authoritative_name')
    config.add_facet_field 'name_first_char_ss', label: 'A-Z', limit: 50, sort: 'index'

    custom_solr_getter config, slug_field: 'all_slugs_sms', fq: PersonSearchBuilder.filters

    # Show Fields
    config.show.html_title_field = 'title'

    config.add_show_field :authoritative_name_ss, label: I18n.t('person.fields.authoritative_name')
    config.add_show_field :bio_history_ss, accessor: :bio_history, label: I18n.t('person.fields.bio_history')
    config.add_show_field :alternate_names_sms, label: I18n.t('person.fields.alternate_names')
    config.add_show_field I18n.t('person.fields.subjects_personal'), accessor: :subjects_personal, helper_method: :linkify_subjects_personal
    config.add_show_field I18n.t('grouped_list.non_educator_items'), accessor: :items,
                          helper_method: :person_item_list_non_educator
    config.add_show_field I18n.t('grouped_list.educator_items'), accessor: :educator_resources,
                          helper_method: :person_item_list_educator

  end

  private

  def set_first_character
    if params['q'].present?
      @search_state.params.delete('f')
      return
    end

    if params[:first_char]
      @search_state.params.merge!(f: { name_first_char_ss: [params[:first_char].first.upcase] })
      @page_title = "#{I18n.t('titles.people')} - #{params[:first_char].upcase}"
    else
      @search_state.params.merge!(f: { name_first_char_ss: ['A'] })
      @page_title = "#{I18n.t('titles.people')} - A"
    end
  end

  def set_a_to_z
    @a_to_z = FacetService.get_people_facet_counts('name_first_char_ss')
  end
end
