# frozen_string_literal: true

# States heat map and list
class PlacesController < HomepageController
  def index
    state_facet_counts = FacetService.get_records_facet_counts('us_states_facet')
    @states = state_facet_counts.keys.filter(&:present?).sort.map do |state_name|
      {
        name: state_name,
        count: state_facet_counts[state_name],
        link: search_records_path('f[us_states_facet][]' => state_name)
      }
    end
    @page_title = I18n.t('titles.places')
  end
end
