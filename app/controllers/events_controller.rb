# frozen_string_literal: true

# Blacklight search controller for Collections
class EventsController < CatalogController
  include BlacklightRangeLimit::ControllerOverride
  include HasCustomSolrGetter

  layout 'event_blacklight'
  configure_blacklight do |config|
    config.search_builder_class = EventSearchBuilder

    config.document_model = EventSolrDocument
    config.show.route = { controller: 'events' }

    custom_solr_getter config, slug_field: EventSolrDocument.unique_key, fq: EventSearchBuilder.filters

    # Show Fields
    config.show.html_title_field = 'title'

    config.add_show_field I18n.t('event.fields.title'), accessor: :title, helper_method: :link_to_event_facet_results
    config.add_show_field :dcterms_temporal_sms, label: I18n.t('event.fields.dcterms_temporal')
    config.add_show_field :description_ss, label: I18n.t('event.fields.description')
    config.add_show_field I18n.t('grouped_list.non_educator_items'), accessor: :items,
                          helper_method: :event_item_list_non_educator
    config.add_show_field I18n.t('grouped_list.educator_items'), accessor: :educator_resources,
                          helper_method: :event_item_list_educator
  end

  def index
    super
    @page_title = I18n.t('titles.events')
  end

  def search_action_url(options = {})
    search_records_path(options.except(:controller, :action))
  end

end
