class ContactController < HomepageController

  layout 'blacklight'

  def index
    @page_title = I18n.t('titles.contact')
  end

end
