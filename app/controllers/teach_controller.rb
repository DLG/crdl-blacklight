# frozen_string_literal: true

# "Teach" menu items static content controller
class TeachController < HomepageController
  def using_materials; end
end
