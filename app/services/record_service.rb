# frozen_string_literal: true

# :nodoc:
class RecordService < SolrService
  # @return [Array<SolrDocument>]
  def self.by_record_ids(record_ids, *additional_filters, field_list: nil)
    filters = RecordSearchBuilder.record_filters + additional_filters
    response = get_documents record_ids, *filters, solr_column: SolrDocument.unique_key, field_list: field_list
    get_solr_docs response
  end
end
