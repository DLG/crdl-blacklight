# frozen_string_literal: true

class FeatureFlags
  def self.enabled?(feature_name)
    case feature_name.to_sym
    when :beta_banner
      false
    when :new_search_boxes
      !Rails.env.production?
    else
      false
    end
  end
end
