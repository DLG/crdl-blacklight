# frozen_string_literal: true

# :nodoc:
class InstitutionService < SolrService
  # @return [Array<InstitutionSolrDocument>]
  def self.by_slugs(slugs, *additional_filters, field_list: nil)
    filters = InstitutionSearchBuilder.filters + additional_filters
    response = get_documents slugs, *filters, solr_column: InstitutionSolrDocument.unique_key, field_list: field_list
    get_solr_docs response, InstitutionSolrDocument
  end
end
