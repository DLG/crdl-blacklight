# frozen_string_literal: true

# Represent an Event from Solr
class PersonSolrDocument < SolrDocument

  self.unique_key = 'slug_ss'

  # @return [String]
  def slug
    self[:slug_ss]
  end

  # @return [Array<String>] All Slugs
  def all_slugs
    self[:all_slugs_sms]
  end

  # @return [String] Title
  def title
    self[:title]
  end

  # @return [String] authoritative_name
  def authoritative_name
    self[:authoritative_name_ss]
  end

  # @return [Array<String>] alternate_names
  def alternate_names
    self[:alternate_names_sms]
  end

  # @return [String] name_first_char
  def name_first_char
    self[:name_first_char_ss]
  end

  # @return [Array<String>] related_names
  def related_names
    self[:related_names_sms]
  end

  # @return [String] bio_history
  def bio_history
    self[:bio_history_ss]&.html_safe
  end

  # @return [Array<String>] authoritative_name_source
  def authoritative_name_source
    self[:authoritative_name_source_sms]
  end

  # @return [String] source_uri
  def source_uri
    self[:source_uri_ss]
  end

  # @return [String] source_uri
  def oclc_number
    self[:source_uri_ss]
  end

  # @return [String] source_uri
  def first_char
    self[:name_first_char_ss]
  end

  def related_items(*additional_filters)
    query = {
      'fq' => RecordSearchBuilder.item_filters
    }
    query['fq'] << "name_slug_sms:\"#{slug}\""
    query['fq'] += additional_filters
    query['rows'] = 5000
    response = Blacklight.default_index.connection.get 'select', params: query
    response&.dig('response', 'docs').map { |doc| SolrDocument.new doc }
  end

  def items
    related_items 'educator_resource_b:false'
  end

  def educator_resources
    related_items 'educator_resource_b:true'
  end

  def fetch_subjects_personal
    likely_subjects = FacetService.search_facet_values('subject_personal_facet',
                                                       authoritative_name,
                                                       nil,
                                                       *RecordSearchBuilder.record_filters).keys
    exact_match_or_qualifier = Regexp.new "^#{Regexp.escape authoritative_name}($| ?--)"
    likely_subjects.filter{ |s| exact_match_or_qualifier.match? s }.sort
  end

  def subjects_personal
    @subjects_personal ||= fetch_subjects_personal
  end

  def items_json(items)
    items.map do |item|
      {
        record_id: item.id,
        record_class: item.klass,
        title: item.title,
        mediums: item.dcterms_medium,
        dcterms_description: item.dcterms_description
      }
    end
  end

  def as_json(_)
    {
      person_slug: slug,
      title: title,
      authorataive_name: authoritative_name,
      biography: bio_history,
      alternate_names: alternate_names,
      records: items_json(items),
      educator_resources: items_json(educator_resources)
    }
  end

end
