# frozen_string_literal: true

# Custom SearchBuilder for use in event-only blacklight pages
class EventSearchBuilder < SearchBuilder
  self.default_processor_chain += %i[return_all_rows]

  def self.filters
    base_filters + ['class_name_ss:Event']
  end

  def show_only_desired_classes(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << 'class_name_ss:Event'
  end

  def return_all_rows(solr_parameters)
    solr_parameters[:start] = 0
    solr_parameters[:rows]  = 1000
  end
end
