# frozen_string_literal: true
class SearchBuilder < Blacklight::SearchBuilder
  include Blacklight::Solr::SearchBuilderBehavior
  include BlacklightAdvancedSearch::AdvancedSearchBuilder
  self.default_processor_chain += %i[add_advanced_parse_q_to_solr add_advanced_search_to_solr]
  include BlacklightRangeLimit::RangeLimitBuilder

  # These are filters that are to the facet query (fq) for all classes (Items, Collections, Names, Events)
  def self.base_filters
    [
      'display_b:1',        # Limit to public objects
      'portals_sms:crdl'  # Limit to objects in CRDL portal
    ]
  end

  self.default_processor_chain.insert 0, :correct_year_facet_inputs
  self.default_processor_chain += %i[
    apply_base_filters
    show_only_desired_classes
    stringify_year_facet_range_query
  ]



  # overridden to to correct switch begin/end year if they're out of order before they get added to solr
  def correct_year_facet_inputs(solr_params)
    year_facet = blacklight_params.dig('range', 'year_facet')
    if year_facet.present?
      begin_range, end_range = year_facet['begin'], year_facet['end']
      if begin_range =~ /^\d+$/ && end_range =~ /^\d+$/ && begin_range.to_i(10) > end_range.to_i(10)
        begin_range, end_range = end_range, begin_range
      elsif begin_range =~ /^\d+$/ && end_range.blank?
        end_range = '9999'
      elsif end_range =~ /^\d+$/ && begin_range.blank?
        begin_range = '0000'
      end
      year_facet['begin'], year_facet['end'] = begin_range, end_range
    end
  end

  def apply_base_filters(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] += self.class.base_filters
  end

  def stringify_year_facet_range_query(solr_parameters)
    solr_parameters.delete 'facet.query' # the same stuff is in fq
    solr_parameters[:fq].each_with_index do |f, i|
      if f =~ /^year_facet:\s*\[\s*(\d+)\s+TO\s+(\d+)\s*\]$/
        begin_range, end_range = $1, $2
        solr_parameters[:fq][i] = "year_facet:[#{begin_range.rjust(4, '0')} TO #{end_range.rjust(4, '0')}]"
      end
    end
  end

  def show_only_desired_classes(solr_parameters)
    solr_parameters[:fq] ||= []
  end
end
