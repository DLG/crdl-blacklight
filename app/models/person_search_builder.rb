# frozen_string_literal: true

# Custom SearchBuilder for use in event-only blacklight pages
class PersonSearchBuilder < SearchBuilder
  self.default_processor_chain += %i[return_all_rows]

  def self.filters
    base_filters + [
      'class_name_ss:Name',
      'name_type_ss:person'
    ]
  end

  def show_only_desired_classes(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << 'class_name_ss:Name'
    solr_parameters[:fq] << 'name_type_ss:person'
  end

  def return_all_rows(solr_parameters)
    solr_parameters[:start] = 0
    solr_parameters[:rows]  = 100000
  end
end
