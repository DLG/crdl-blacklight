# frozen_string_literal: true

# Represent an Event from Solr
class EventSolrDocument < SolrDocument

  self.unique_key = 'slug_ss'

  # @return [String]
  def slug
    self[:slug_ss]
  end

  # @return [String] Title Type
  def title
    self[:title]
  end

  # @return [Array<String>] Years
  def dcterms_temporal
    self[:dcterms_temporal_sms]
  end

  # @return [String] Description
  def description
    self[:description_ss]
  end

  def related_items(*additional_filters)
    query = {
      'fq' => RecordSearchBuilder.item_filters
    }
    query['fq'] << "event_title_sms:\"#{title}\""
    query['fq'] += additional_filters
    query['rows'] = 5000
    response = Blacklight.default_index.connection.get 'select', params: query
    response&.dig('response', 'docs').map { |doc| SolrDocument.new doc }
  end

  def items
    related_items 'educator_resource_b:false'
  end

  def educator_resources
    related_items 'educator_resource_b:true'
  end

  def items_json(items)
    items.map do |item|
      {
        record_id: item.id,
        record_class: item.klass,
        title: item.title,
        mediums: item.dcterms_medium,
        dcterms_description: item.dcterms_description
      }
    end
  end

  def as_json(_)
    {
      event_id: id,
      title: title,
      years: dcterms_temporal,
      description: description,
      records: items_json(items),
      educator_resources: items_json(educator_resources)
    }
  end

  def self.all_years(events)
    years = []
    events.each do |event|
      years.push(*event.dcterms_temporal.map(&:strip))
    end
    years.uniq.compact.sort
  end

  def self.event_index_hash(response)
    year_hash = {}
    response.docs.each do |event|
      event.dcterms_temporal.each do |year|
        year = year.strip
        existing_array = year_hash[year]
        if existing_array
          existing_array.push event
        else
          year_hash[year] = [event]
        end
      end
    end
    year_hash
  end
end
