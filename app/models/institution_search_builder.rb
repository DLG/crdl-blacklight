# frozen_string_literal: true

# Custom SearchBuilder for use in Institution blacklight pages
class InstitutionSearchBuilder < SearchBuilder
  self.default_processor_chain += %i[return_all_rows]

  def self.filters
    base_filters + %w[class_name_ss:HoldingInstitution]
  end

  def show_only_desired_classes(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << 'class_name_ss:HoldingInstitution'
  end

  def return_all_rows(solr_parameters)
    solr_parameters[:start] = 0
    solr_parameters[:rows]  = 100_000
  end
end
