json.response do
  json.docs @presenter.documents do |document|
    json.event_id document.id
    json.title document.title
    json.years document.dcterms_temporal
    json.description document.description
  end
  json.facets @presenter.search_facets&.map { |f| f.as_json }
                                      &.each { |f| f&.dig('options')&.delete 'response' }
  json.pages @presenter.pagination_info
end
