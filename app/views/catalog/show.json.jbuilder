# frozen_string_literal: true

json.response do
  json.document @document
end