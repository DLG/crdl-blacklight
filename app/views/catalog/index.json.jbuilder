json.response do
  #region Blacklight defaults:
  json.docs @presenter.documents
  # json.facets @presenter.search_facets
  json.pages @presenter.pagination_info
  #endregion

  # Below is our addition so range mins/maxes are provided in API output
  facets = @presenter.search_facets&.map { |f| f.as_json }
                                   &.each { |f| f&.dig('options')&.delete 'response' }
                                   &.index_by { |f| f['name'] }
  continue unless facets
  stats_fields = @response&.dig 'stats', 'stats_fields'
  stats_fields&.each do |field, stats|
    facets[field] = facets[field]&.merge stats
  end
  json.facets facets.values
end