json.response do
  json.docs @presenter.documents do |document|
    json.slug document.slug
    json.authoritative_name document.authoritative_name
    json.biography document.bio_history
    json.alternate_names document.alternate_names
  end
  json.facets @presenter.search_facets&.map { |f| f.as_json }
                                      &.each { |f| f&.dig('options')&.delete 'response' }
  json.pages @presenter.pagination_info
end
