//= require jquery
//= require 'blacklight_advanced_search'


//= require rails-ujs
//= require turbolinks
//
// Required by Blacklight
//= require popper
// Twitter Typeahead for autocomplete
//= require twitter/typeahead
//= require bootstrap
//= require blacklight/blacklight
//= require blacklight_gallery

// For blacklight_range_limit built-in JS, if you don't want it you don't need
// this:
//= require 'blacklight_range_limit'

Blacklight.onLoad(function() {
    $('#close-harmful-content-modal').click(function() {
        $(this).closest(".modal").modal("hide");
        $('.modal-backdrop').css( "display", "none")
    });

    var modalOpened = getCookie("modal_open");
    if ((modalOpened !== "yes") && (window.location.pathname === '/')) {
        $('.harmful-content-modal').modal({
            open: true
        })
        modalOpened = "yes";
        setCookie("modal_open", modalOpened, 30);
    }

    $('[data-grouped-items-expand-all]').click(function (e) {
        var action = $(this).attr('data-grouped-items-expand-all');
        $(this).closest('dd').find('.grouped-items-accordion .collapse').collapse(action);
        $(this).text(action === 'show' ? 'collapse all' : 'expand all');
        $(this).attr('data-grouped-items-expand-all', action === 'show' ? 'hide' : 'show')
        e.preventDefault();
        return false;
    });

    $('body[data-spy="scroll"]').scrollspy('refresh');

    //Remove thumbnail img if thumbnail fails to load
    $('img.thumbnail, .thumbnail>img').on('error', function() {
        this.remove();
    });
});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
