// NOTE: This file is not currently used. The only global JS is still via sprockets
// You probably want app/assets/javascript/application.js

// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

// Vendor
require('@rails/ujs').start()
global.Rails = Rails

import Rails from "@rails/ujs"
import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import "channels"
import "bootstrap/dist/js/bootstrap"
import 'blacklight-frontend/app/assets/javascripts/blacklight/blacklight'

Rails.start()
Turbolinks.start()
ActiveStorage.start()
