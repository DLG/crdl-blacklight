// Note: we already expect jQuery and Blacklight to be available as window.$ and window.Blacklight

// Import leaflet. The code below imports it as an ES6 Module
// Alternatively, simply doing `import leaflet` will add leaflet to the global scope as window.L
import * as Leaflet from 'leaflet/dist/leaflet-src.esm'
import 'leaflet/dist/leaflet.css'

const colorRamp = [
    [0,    '#E4D9DA'], // 0
    [1,    '#C8AEB8'], // 1 - 99
    [100,  '#B692A2'], // 100 - 299
    [300,  '#A4768C'], // 300 - 999
    [1000, '#915975'], // 1000 - 2999
    [3000, '#7F3D5F']  // 3000+
];

const sortFunctions = {
    alphabetic: function (a, b) {
        const aName = $(a).data('state');
        const bName = $(b).data('state');
        if (aName < bName) return -1;
        if (aName > bName) return 1;
        return 0;
    },
    count: function (a, b) {
        return parseInt($(b).data('count')) - parseInt($(a).data('count'));
    }
};

function createBasicMap(element, center, zoom) {
    const map = Leaflet.map(element, {
        scrollWheelZoom: false,
        center: center,
        zoom: zoom
    });

    // intuitive handling of mousewheel zooming
    map.on('focus', function() { map.scrollWheelZoom.enable(); });
    map.on('blur', function() { map.scrollWheelZoom.disable(); });

    Leaflet.tileLayer(
        'https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png',
        {
            id: 'osm.default',
            attribution: '© OpenStreetMap, © CartoDB'
        }
    ).addTo(map);

    const labelsLayerPane = map.createPane('labels');
    labelsLayerPane.style.zIndex = 650;
    labelsLayerPane.style.pointerEvents = 'none';

    Leaflet.tileLayer('https://{s}.basemaps.cartocdn.com/light_only_labels/{z}/{x}/{y}.png', {
        attribution: '© OpenStreetMap, © CartoDB',
        pane: 'labels'
    }).addTo(map);

    return map;
}

function createStateMap(element) {

    const stateGeoJSONDeferred = $.get($(element).data('state-map'));

    resetPanesAndSelection();
    const map = createBasicMap(element, [37.93, -96.076], 4);

    const stateData= {};
    $('[data-state-list] [data-state]').each(function (i, el) {
        stateData[$(el).data('state')] = {
            count: $(el).data('count'),
            link: $(el).data('link')
        };
        $(el).mouseover(function () {
            selectByName($(this).data('state'));
        });
        $(el).mouseout(function () {
            resetSelection();
        });
    });

    // Add legend and info pane
    const info = Leaflet.control({position: 'topright'});
    const legend = Leaflet.control({ position: 'bottomright' });

    info.onAdd = function (map) {
        this._div = Leaflet.DomUtil.create('div', 'info map-selection');
        this._div.innerHTML = '<div class="no-selection">Select a State for More Info</div>' +
            '<div class="has-selection">' +
                '<div data-selection-name></div>' +
                '<p><span class="badge badge-primary" data-selection-count></span> records</p>' +
                '<a href="#" class="btn btn-outline-primary" data-selection-link>View All Records from <span data-selection-name></span></a>' +
            '</div>'
        return this._div;
    };

    legend.onAdd = function (map) {
        const div = Leaflet.DomUtil.create('div', 'info legend');
        div.innerHTML += '<p><strong>Number of Records</strong></p>';
        for (let i = 1; i < colorRamp.length; i++) {
            div.innerHTML +=
                '<i style="background:' + colorRamp[i][1] + '"></i> ' +
                colorRamp[i][0] + (colorRamp[i + 1] ? '&ndash;' + (colorRamp[i + 1][0] - 1) + '<br>' : '+');
        }
        return div
    };

    info.update = function (props) {
        if (props) {
            const $info = $(this._div)
            $info.attr('data-has-selection', true);
            $info.find('[data-selection-name]').text(props['NAME']);
            $info.find('[data-selection-link]').attr('href', props['link']);
            $info.find('[data-selection-count]').text(props['count'].toLocaleString());
        } else {
            $(this._div).removeAttr('data-has-selection');
        }
    };

    info.addTo(map);
    legend.addTo(map);

    const featuresByName = {};
    const geoJSON = Leaflet.geoJSON(null, {
        style: style,
        onEachFeature: function (feature, layer) {
            featuresByName[feature.properties['NAME']] = layer;
            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                click: selectFeature
            });
        }
    }).addTo(map);

    stateGeoJSONDeferred.then(function (result) {
        result.features.forEach(function (feat) {
            let data = stateData[feat.properties['NAME']];
            for (let key in data) {
                feat.properties[key] = data[key];
            }
        });
        geoJSON.addData(result);
    });

    // region GeoJSON layer styles and event-handling
    function getColor(d) {
        let i;
        for (i = 0; i < colorRamp.length - 1; i++) {
            if (d < colorRamp[i + 1][0]) break;
        }
        return colorRamp[i][1];
    }

    function style(feature) {
        return {
            fillColor: getColor(feature.properties.count),
            weight: 1,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.7
        }
    }

    function highlightFeature(e) {
        const layer = e.target;

        layer.setStyle({
            weight: 3,
            color: '#666',
            dashArray: ''
        });

        if (!Leaflet.Browser.ie && !Leaflet.Browser.opera && !Leaflet.Browser.edge) {
            layer.bringToFront();
        }

        listElementByName(layer.feature.properties['NAME']).addClass('highlight');
    }

    function resetHighlight(e) {
        geoJSON.resetStyle(e.target);
        listElementByName(e.target.feature.properties['NAME']).removeClass('highlight');
    }
    // endregion

    // region selection functions
    let lastSelection;
    function selectFeature(e) {
        unselect(lastSelection);
        if (e.target && e.target.feature) {
            highlightFeature(e);
            listElementByName(e.target.feature.properties['NAME']).addClass('selected');
            info.update(e.target.feature.properties);
            lastSelection = e.target;
        }
    }

    function unselect(target) {
        if (target) {
            resetHighlight({target: target});
            listElementByName(target.feature.properties['NAME']).removeClass('selected');
        }
    }


    function resetSelection() {
        unselect(lastSelection);
        info.update(null);
    }

    function selectByName(name) {
        const feature = featuresByName[name];
        if (feature) selectFeature({ target: feature });
    };

    function listElementByName(name) {
        return $('[data-state-list] [data-state="' + name + '"]');
    }
    // endregion

    function resetPanesAndSelection() { // Deal with some turbolinks issues
        $('[data-state-list] [data-state]').removeClass('highlight').removeClass('selected');
        $('[data-state-map] .info').each(function (i, el){
            $(el.parentElement).remove();
        });
    }

    return map;
}

function sortList(sortMode) {
    const $list = $('[data-state-list]');
    const $items = $list.find('[data-state]');
    $items.sort(sortFunctions[sortMode]).appendTo($list);
    $('[data-sort]:not([data-sort=' + sortMode + '])').removeClass('active');
    $('[data-sort=' + sortMode + ']').addClass('active');
}

function initialize() {
    $('[data-state-map]').each(function (i, el) {
        createStateMap(el);
        $('[data-sort]').click(function () {
            sortList($(this).data('sort'));
            return false;
        });
        $(el).attr('data-map-ready', true);
    })
}
Blacklight.onLoad(initialize);
initialize();