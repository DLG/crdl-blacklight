# frozen_string_literal: true

# Class to send logs to galileo-admin-logs
class Notifier
  NOTIFICATION_ENVIRONMENTS = %w[production staging development dev].freeze

  def initialize
    webhook_url = Rails.application.credentials.slack_notifier_webhook
    @notifier = Slack::Notifier.new webhook_url
  end

  # Sends message to console and sends passed message to galileo-admin-logs channel
  def send(message)
    hostname = if Rails.env == "development"
                 " #{Socket.gethostname}"
               else
                 ''
               end
    puts message
    @notifier.ping("`#{Rails.env}#{hostname}`: #{message}") if NOTIFICATION_ENVIRONMENTS.include? Rails.env
  end
end
